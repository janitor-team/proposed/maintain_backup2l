# Submitted by: Joachim Wiedorn  (2011-02-20)

# example driver with modern xz compression. Please note, that 
# the xz driver have quality 1...9, but only the first one should
# be used with backup2l (1, 2 or 3) because higher qualities are
# too slow for using for backup (Default quality of xz is "6").

# Use tar with the new driver XZ, here with the faster quality "3"
DRIVER_MY_TAR_XZ ()
{
    case $1 in
        -test)
            require_tools tar xz-utils
            echo "ok"
            ;;
        -suffix)
            echo "tar.xz"
            ;;
        -create)        # Arguments: $2 = BID, $3 = archive file, $4 = filelist file
            tar c --no-recursion -T $4 | xz -z -3 > $3  2>&1
            ;;
        -toc)           # Arguments: $2 = BID, $3 = archive file
            tar tJf $3 | sed 's#^#/#'
            ;;
        -extract)       # Arguments: $2 = BID, $3 = archive file, $4 = filelist file
            tar xJf $3 --same-permission --same-owner -T $4 2>&1
            ;;
    esac
}
